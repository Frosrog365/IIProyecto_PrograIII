﻿using BlackJackEntities;
using System;
using BlackJackDAL;

namespace BlackJackBOL
{
    public class JuegoBOL
    {
        
        public JuegoBOL()
        {
        }

        public EPartida BuscarPublica(EUsuario usuario, EPartida partida)
        {
            return new JuegoDAL().Join(usuario, partida);
        }

        public EPartida CargarPartida(EPartida partida)
        {
            return new JuegoDAL().CargarPartidaID(partida.Id);
        }

        public void Plantarse(EPartida partida)
        {
            new JuegoDAL().Actualizar(partida);
        }

        public void BorrarPartida(EUsuario jugador)
        {
            new JuegoDAL().BorrarPartida(jugador);
        }
    }
}