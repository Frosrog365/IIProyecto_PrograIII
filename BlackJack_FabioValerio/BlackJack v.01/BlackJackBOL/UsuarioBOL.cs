﻿using BlackJackDAL;
using BlackJackEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlackJackBOL
{
    public class UsuarioBOL
    {
        UsuarioDAL dal = new UsuarioDAL();

        public int cargarFondos(EUsuario jugador)
        {
            return dal.cargarFondos(jugador);
        }

        public bool ModificarFondos(EUsuario jugador)
        {
            return dal.ModificarFondos(jugador);
        }

        public bool Verificar(EUsuario jugador)
        {
            return dal.Verificar(jugador);
        }

        public EUsuario EstaJugando(EUsuario jugador)
        {
            return dal.EstaJugando(jugador);
        }

        public void DejarJugar(EUsuario jugador)
        {
           dal.DejarJugar(jugador);
        }
    }
}
