﻿namespace BlackJackEntities
{
    public class EPartida
    {
        public int Id { get; set; }
        public int CantJugadores { get; set; }
        public int JugadorActual { get; set; }
        public string Password { get; set; }
    }
}