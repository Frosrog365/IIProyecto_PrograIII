﻿namespace FB_Analyze
{
    partial class FB_Analyze
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnFacebookLogin = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnFacebookLogin
            // 
            this.btnFacebookLogin.BackColor = System.Drawing.Color.Transparent;
            this.btnFacebookLogin.BackgroundImage = global::FirstFacebookApplication.Properties.Resources.facebook_sign_in_button1;
            this.btnFacebookLogin.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnFacebookLogin.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.btnFacebookLogin.FlatAppearance.BorderSize = 2;
            this.btnFacebookLogin.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.btnFacebookLogin.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.btnFacebookLogin.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnFacebookLogin.Location = new System.Drawing.Point(108, 232);
            this.btnFacebookLogin.Name = "btnFacebookLogin";
            this.btnFacebookLogin.Size = new System.Drawing.Size(232, 56);
            this.btnFacebookLogin.TabIndex = 1;
            this.btnFacebookLogin.UseVisualStyleBackColor = false;
            this.btnFacebookLogin.Click += new System.EventHandler(this.btnFacebookLogin_Click);
            // 
            // FB_Analyze
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::FirstFacebookApplication.Properties.Resources.sdasdasd;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(448, 300);
            this.Controls.Add(this.btnFacebookLogin);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.Name = "FB_Analyze";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "BlackJack V.01 Login";
            this.Load += new System.EventHandler(this.FB_Analyze_Load);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Button btnFacebookLogin;
    }
}

