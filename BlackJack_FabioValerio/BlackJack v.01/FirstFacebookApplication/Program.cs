﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace FB_Analyze
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            FB_Analyze frm = new FB_Analyze();
            frm.Show();
            Application.Run(new FB_Analyze());
        }
    }
}
