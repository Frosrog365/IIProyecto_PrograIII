﻿namespace FB_Analyze
{
    using System;
    using System.Windows.Forms;
    using BlackJack_FabioValerio;
    using BlackJackBOL;
    using BlackJackEntities;
    using Facebook;

    public partial class FB_Analyze : Form
    {
        private const string AppId = "167644310606790";
        private const string ExtendedPermissions = "public_profile,email";
        private string _accessToken;
        private EUsuario jugador;


        public FB_Analyze()
        {
            InitializeComponent();
        }

        private void btnFacebookLogin_Click(object sender, EventArgs e)
        {
            var fbLoginDialog = new FB_LoginDialog(AppId, ExtendedPermissions);
            fbLoginDialog.ShowDialog();

            DisplayAppropriateMessage(fbLoginDialog.FacebookOAuthResult);
        }

        private void DisplayAppropriateMessage(FacebookOAuthResult facebookOAuthResult)
        {
            if (facebookOAuthResult != null)
            {
                if (facebookOAuthResult.IsSuccess)
                {
                    _accessToken = facebookOAuthResult.AccessToken;
                    var fb = new FacebookClient(facebookOAuthResult.AccessToken);

                    //string imgURL = String.Format("https://graph.facebook.com/{0}/picture", Obtener_Usuario(_accessToken));

                    //pictureBox1.Load(imgURL);
                    //fbAnalyze.Show();
                    jugador = new EUsuario();
                    jugador.Nombre = ObtenerNombre(_accessToken).ToString();
                    string imgURL = String.Format("https://graph.facebook.com/{0}/picture?type=large", ObtenerID(_accessToken));
                    jugador.Correo = Obtener_Email(_accessToken).ToString();
                    new UsuarioBOL().Verificar(jugador);
                    if (new UsuarioBOL().EstaJugando(jugador).Jugando)
                    {
                        MessageBox.Show("Ese usuario está jugando");
                        Cerrar();
                    }
                    else
                    {
                        jugador.Jugando = true;
                        new UsuarioBOL().DejarJugar(jugador);
                        BlackJackMenu juego = new BlackJackMenu(jugador, imgURL);
                        juego.Show(this);
                        Cerrar();
                        Hide();
                    }
                }
                else
                {
                    MessageBox.Show(facebookOAuthResult.ErrorDescription);
                }
            }
        }


        public static string ObtenerID(string AccessToken)
        {
            FacebookClient fb = new FacebookClient(AccessToken);
            dynamic MyData = fb.Get("/me");
            return MyData.id;
        }

        public static string ObtenerNombre(string AccessToken)
        {
            FacebookClient fb = new FacebookClient(AccessToken);
            dynamic MyData = fb.Get("/me");
            return MyData.name;
        }

        public static string Obtener_Email(string AccessToken)
        {
            FacebookClient fb = new FacebookClient(AccessToken);
            dynamic result = fb.Get("me?fields=id,name,email");
            return result.email;
        }

        private void Cerrar()
        {
            var webBrowser = new WebBrowser();
            var fb = new FacebookClient();
            var logouUrl = fb.GetLogoutUrl(new { access_token = _accessToken, next = "https://www.facebook.com/connect/login_success.html" });
            webBrowser.Navigate(logouUrl);
            
        }

        private void FB_Analyze_Load(object sender, EventArgs e)
        {

        }
    }
}
