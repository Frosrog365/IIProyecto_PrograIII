﻿using BlackJackEntities;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlackJackDAL
{
    public class GananciaDAL
    {
        public List<EGanancias> CargarTodo(EUsuario usuario)
        {
            List<EGanancias> ganancias = new List<EGanancias>();
            using (NpgsqlConnection con = new NpgsqlConnection(Configuracion.ConStr))
            {
                con.Open();
                string sql = @"select id, id_usu, ganancia from ganancias where id_usu = @id;";
                NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
                cmd.Parameters.AddWithValue("@id", usuario.Id);
                NpgsqlDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    ganancias.Add(CargarGanancia(reader));
                }

            }

            return ganancias;
        }
        private EGanancias CargarGanancia(NpgsqlDataReader reader)
        {
            EGanancias gan = new EGanancias
            {
                Id = reader["id"] != DBNull.Value ? Convert.ToInt32(reader["id"]) : 0,
                Id_usu = Convert.ToInt32(reader["id_usu"].ToString()),
                ganancia = Convert.ToInt32(reader["ganancia"].ToString()),
            };

            return gan;
        }

        public bool Insertar(EGanancias gan)
        {
            using (NpgsqlConnection con = new NpgsqlConnection(Configuracion.ConStr))
            {
                con.Open();
                string sql = @"insert into ganancias(id_usu,ganancia) values(@id_usu,@ganancia)";
                NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
                cmd.Parameters.AddWithValue("@id_usu", gan.Id_usu);
                cmd.Parameters.AddWithValue("@ganancia", gan.ganancia);

                return cmd.ExecuteNonQuery() > 0;
            }
        }
    }
}
