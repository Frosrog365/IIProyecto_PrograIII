﻿using BlackJackDAL;
using BlackJackEntities;
using Npgsql;
using System;
using System.Collections.Generic;

namespace BlackJackDAL
{
    public class JuegoDAL
    {
        public EPartida Join(EUsuario usuario, EPartida partida)
        {
            try
            {
                using (NpgsqlConnection con = new NpgsqlConnection(Configuracion.ConStr))
                {
                    //Abrir una conexion
                    con.Open();
                    //Definir la consulta
                    string sql = @"insert into par_usu(id_jug, id_par) 
                               values (@id,
                                      (select id from partida where pass like @pass limit 1)) 
                               returning id_par";
                    NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
                    cmd.Parameters.AddWithValue("@id", usuario.Id);
                    cmd.Parameters.AddWithValue("@pass", partida.Password);
                    int idPar = Convert.ToInt32(cmd.ExecuteScalar());
                    return partida = CargarPartidaID(idPar);
                }
            }
            catch (Exception)
            {
                return null;
            }
        }

        public EPartida CrearPartida(EPartida partida)
        {
            using (NpgsqlConnection con = new NpgsqlConnection(Configuracion.ConStr))
            {
                //Abrir una conexion
                con.Open();
                //Definir la consulta
                string sql = @"insert into partida(pass,cant_jug,jug_actual) values(@pass, @cant, @jug) returning id";
                NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
                cmd.Parameters.AddWithValue("@pass", partida.Password);
                cmd.Parameters.AddWithValue("@cant", partida.CantJugadores);
                cmd.Parameters.AddWithValue("@jug", partida.JugadorActual);

                int idPar = Convert.ToInt32(cmd.ExecuteScalar());
                partida.Id= idPar;
                return CargarPartidaID(idPar);
            }
        }

        public void ResetearPartida()
        {
            using (NpgsqlConnection con = new NpgsqlConnection(Configuracion.ConStr))
            {
                //Abrir una conexion
                con.Open();
                //Definir la consulta
                string sql = @"delete from mazo;
                                delete from cartas_dealer;";
                NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
                NpgsqlDataReader reader = cmd.ExecuteReader();
                if (reader.Read())
                {

                }
            }
        }
        public void BorrarPartida(EUsuario jugador)
        {
            using (NpgsqlConnection con = new NpgsqlConnection(Configuracion.ConStr))
            {
                //Abrir una conexion
                con.Open();
                //Definir la consulta
                string sql = @"delete from par_usu where id_jug = @id";
                NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
                cmd.Parameters.AddWithValue("@id", jugador.Id);
                NpgsqlDataReader reader = cmd.ExecuteReader();
                if (reader.Read())
                {

                }
            }
        }

        public EPartida CargarPartidaID(int idPar)
        {
            using (NpgsqlConnection con = new NpgsqlConnection(Configuracion.ConStr))
            {
                //Abrir una conexion
                con.Open();
                //Definir la consulta
                string sql = @"select id,pass, cant_jug, jug_actual from partida 
                        where id = @id";
                NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
                cmd.Parameters.AddWithValue("@id", idPar);
                NpgsqlDataReader reader = cmd.ExecuteReader();

                if (reader.Read())
                {
                    return CargarPartida(reader);
                }
            }
            return null;

        }

        private EPartida CargarPartida(NpgsqlDataReader reader)
        {
            return new EPartida
            {
                Id = reader.GetInt32(0),
                Password = reader.GetString(1),
                CantJugadores = reader.GetInt32(2),
                JugadorActual = reader.GetInt32(3)
            };
        }

        private int Siguiente(EPartida partida)
        {
            using (NpgsqlConnection con = new NpgsqlConnection(Configuracion.ConStr))
            {
                //Abrir una conexion
                con.Open();
                //Definir la consulta
                string sql = @"select id_jug from par_usu
                                where id_par = @idP and orden > (select orden from par_usu where id_jug = @idJ and id_par = @idP)
                                order by orden limit 1";
                NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
                cmd.Parameters.AddWithValue("@idP", partida.Id);
                cmd.Parameters.AddWithValue("@idJ", partida.JugadorActual);
                try
                {
                    object o = cmd.ExecuteScalar();
                    int id = o == DBNull.Value ? 0 : Convert.ToInt32(o);
                    if (id > 0)
                    {
                        return id;
                    }
                    cmd.Parameters.Clear();
                    sql = @"select id_jug from par_usu
                                where id_par = @idP 
                                order by orden limit 1";
                    cmd = new NpgsqlCommand(sql, con);
                    cmd.Parameters.AddWithValue("@idP", partida.Id);
                    o = cmd.ExecuteScalar();
                    id = o == DBNull.Value ? 0 : Convert.ToInt32(o);
                    return id;
                }
                catch (Exception ex)
                {
                    throw ex;
                }

            }
        }

        public Card SacarCartaDealer()
        {
            using (NpgsqlConnection con = new NpgsqlConnection(Configuracion.ConStr))
            {
                //Abrir una conexion
                con.Open();
                //Definir la consulta
                string sql = @"select * from cartas_dealer order by id DESC limit 1";
                NpgsqlCommand cmd = new NpgsqlCommand(sql, con);


                NpgsqlDataReader reader = cmd.ExecuteReader();
                if (reader.Read())
                {
                    Card c = new Card();
                    c.image = reader["carta"].ToString();
                    c.value = reader["valor"].ToString();
                    return c;
                }
                else
                {
                    return null;
                }


            }
        }

        public void Actualizar(EPartida partida)
        {
            using (NpgsqlConnection con = new NpgsqlConnection(Configuracion.ConStr))
            {
                //Abrir una conexion
                con.Open();
                //Definir la consulta
                string sql = @"update partida set jug_actual = @sig where id = @id";
                NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
                cmd.Parameters.AddWithValue("@id", partida.Id);
                cmd.Parameters.AddWithValue("@sig", Siguiente(partida));
                cmd.ExecuteNonQuery();
            }
        }
        public void InsertarCarta(Card carta, EUsuario jugador)
        {
            using (NpgsqlConnection con = new NpgsqlConnection(Configuracion.ConStr))
            {
                //Abrir una conexion
                con.Open();
                //Definir la consulta
                string sql = @"insert into mazo(id_jug,carta,valor) values(@id,@carta,@valor)";
                NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
                cmd.Parameters.AddWithValue("@id", jugador.Id);
                cmd.Parameters.AddWithValue("@carta", carta.image);
                cmd.Parameters.AddWithValue("@valor", carta.value);
                cmd.ExecuteNonQuery();
            }
        }
        public string SacarCarta(EUsuario jugador)
        {
            using (NpgsqlConnection con = new NpgsqlConnection(Configuracion.ConStr))
            {
                //Abrir una conexion
                con.Open();
                //Definir la consulta
                string sql = @"select * from mazo where id_jug != @id order by id DESC limit 1";
                NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
                cmd.Parameters.AddWithValue("@id", jugador.Id);

                NpgsqlDataReader reader = cmd.ExecuteReader();
                if (reader.Read())
                {
                    return reader["carta"].ToString();
                }
                else
                {
                    return null;
                }


            }
        }
        public void InsertarCartaDealer(Card urlCarta)
        {
            using (NpgsqlConnection con = new NpgsqlConnection(Configuracion.ConStr))
            {
                //Abrir una conexion
                con.Open();
                //Definir la consulta
                string sql = @"insert into cartas_dealer(carta, valor) values(@carta,@valor)";
                NpgsqlCommand cmd = new NpgsqlCommand(sql, con);

                cmd.Parameters.AddWithValue("@carta", urlCarta.image);
                cmd.Parameters.AddWithValue("@valor", urlCarta.value);
                cmd.ExecuteNonQuery();
            }


        }

        public List<int> SacarJugadores()
        {

            using (NpgsqlConnection con = new NpgsqlConnection(Configuracion.ConStr))
            {
                List<int> jugadores = new List<int>();
                //Abrir una conexion
                con.Open();
                //Definir la consulta
                string sql = @"select * from par_usu order by id";
                NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
                NpgsqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    jugadores.Add(int.Parse(reader["id_jug"].ToString()));
                }
                return jugadores;
            }
        }

    }
}



/*
 CREATE TABLE public.par_usu
(
    id serial,
    id_jug integer NOT NULL,
    id_par integer NOT NULL,
    orden serial,
    CONSTRAINT par_usu_pkey PRIMARY KEY (id),
    CONSTRAINT fk_parusu_par FOREIGN KEY (id_jug)
        REFERENCES public.usuario (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)

CREATE TABLE public.partida
(
    id serial,
    pass text COLLATE pg_catalog."default",
    cant_jug integer,
    jug_actual integer,
    CONSTRAINT partida_pkey PRIMARY KEY (id)
)

 */
