﻿using BlackJackEntities;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlackJackDAL
{
    public class PerdidaDAL
    {
        public List<EPerdidas> CargarTodo(EUsuario usuario)
        {
            List<EPerdidas> ganancias = new List<EPerdidas>();
            using (NpgsqlConnection con = new NpgsqlConnection(Configuracion.ConStr))
            {
                con.Open();
                string sql = @"select id, id_usu, perdida from perdidas where id_usu = @id;";
                NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
                cmd.Parameters.AddWithValue("@id", usuario.Id);
                NpgsqlDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    ganancias.Add(CargarGanancia(reader));
                }

            }

            return ganancias;
        }
        private EPerdidas CargarGanancia(NpgsqlDataReader reader)
        {
            EPerdidas gan = new EPerdidas
            {
                Id = reader["id"] != DBNull.Value ? Convert.ToInt32(reader["id"]) : 0,
                Id_usu = Convert.ToInt32(reader["id_usu"].ToString()),
                perdida = Convert.ToInt32(reader["perdida"].ToString()),
            };

            return gan;
        }

        public bool Insertar(EPerdidas gan)
        {
            using (NpgsqlConnection con = new NpgsqlConnection(Configuracion.ConStr))
            {
                con.Open();
                string sql = @"insert into perdidas(id_usu,perdida) values(@id_usu,@perdida)";
                NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
                cmd.Parameters.AddWithValue("@id_usu", gan.Id_usu);
                cmd.Parameters.AddWithValue("@perdida", gan.perdida);

                return cmd.ExecuteNonQuery() > 0;
            }
        }
    }
}
