﻿using BlackJackEntities;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlackJackDAL
{
    public class UsuarioDAL
    {
        public int cargarFondos(EUsuario u)
        {
            int fondo = 0;
            using (NpgsqlConnection con = new NpgsqlConnection(Configuracion.ConStr))
            {

                con.Open();

                string sql = @"select id, fondos from usuario where email = @email";
                NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
                cmd.Parameters.AddWithValue("@email", u.Correo);
                NpgsqlDataReader reader = cmd.ExecuteReader();

                if (reader.Read())
                {
                    fondo = int.Parse(reader["fondos"].ToString());
                    u.Id = int.Parse(reader["id"].ToString());
                }

            }

            return fondo;
        }

        public void DejarJugar(EUsuario jugador)
        {
            using (NpgsqlConnection con = new NpgsqlConnection(Configuracion.ConStr))
            {

                con.Open();
                string sql = @"UPDATE usuario set jugando = @jugando WHERE email=@email";
                NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
                cmd.Parameters.AddWithValue("@email", jugador.Correo);
                if (jugador.Jugando)
                {
                    cmd.Parameters.AddWithValue("jugando", 1);
                }
                else
                {
                    cmd.Parameters.AddWithValue("jugando", 0);
                }
                cmd.ExecuteNonQuery();
            }
        }

        public EUsuario EstaJugando(EUsuario usuario)
        {
            using (NpgsqlConnection con = new NpgsqlConnection(Configuracion.ConStr))
            {

                con.Open();
                string sql = @"SELECT jugando from usuario WHERE email=@email";
                NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
                cmd.Parameters.AddWithValue("@email", usuario.Correo);
                NpgsqlDataReader reader = cmd.ExecuteReader();
                if (reader.Read())
                {
                    if (int.Parse(reader["jugando"].ToString()) == 0)
                    {
                        usuario.Jugando = false;
                    }
                    else
                    {
                        usuario.Jugando = true;
                    }
                    return usuario;
                }
                return null;
            }
        }

        public bool ModificarFondos(EUsuario usuario)
        {
            using (NpgsqlConnection con = new NpgsqlConnection(Configuracion.ConStr))
            {

                con.Open();
                string sql = @"UPDATE usuario SET fondos=@fondos WHERE email=@email";
                NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
                cmd.Parameters.AddWithValue("@fondos", usuario.Fondos);
                cmd.Parameters.AddWithValue("@email", usuario.Correo);
                return cmd.ExecuteNonQuery() > 0;
            }
        }
        public bool Verificar(EUsuario usuario)
        {
            using (NpgsqlConnection con = new NpgsqlConnection(Configuracion.ConStr))
            {
                //Abrir una conexion
                con.Open();
                //Definir la consulta
                string sql = @"select id,nombre,email,fondos from usuario where email = @email";
                NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
                cmd.Parameters.AddWithValue("@email", usuario.Correo);

                NpgsqlDataReader reader = cmd.ExecuteReader();

                if (reader.Read())
                {
                    return true; ;
                }
                else
                {
                    return Insertar(usuario);
                }
            }

        }
        public bool Insertar(EUsuario usuario)
        {
            using (NpgsqlConnection con = new NpgsqlConnection(Configuracion.ConStr))
            {
                con.Open();
                string sql = @"INSERT INTO usuario(nombre, email, fondos)values 
                                (@nombre,@correo,@fondos)";
                NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
                cmd.Parameters.AddWithValue("@nombre", usuario.Nombre);
                cmd.Parameters.AddWithValue("@correo", usuario.Correo);
                cmd.Parameters.AddWithValue("@fondos", usuario.Fondos);

                return cmd.ExecuteNonQuery() > 0;
            }
        }

    }
}
