﻿using BlackJackDAL;
using BlackJackEntities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BlackJack_FabioValerio
{
    public partial class CrearPartida : Form
    {
        EUsuario jugador;
        private int v;

        public CrearPartida(EUsuario jugador)
        {
            InitializeComponent();
            this.jugador = jugador;
            comboBox1.SelectedIndex = 0;
        }

        public CrearPartida(EUsuario jugador, int v) : this(jugador)
        {
            InitializeComponent();
            this.jugador = jugador;
            comboBox1.SelectedIndex = 0;
            this.v = v;
            comboBox1.Visible = false;
            label2.Visible = false;
            button1.Text = "Unirse";
        }

        private void button1_Click(object sender, EventArgs e)
        {
            
                EPartida partida = new EPartida
                {
                    CantJugadores = int.Parse(comboBox1.SelectedItem.ToString()),
                    JugadorActual = jugador.Id,
                    Password = textBox1.Text
                };
                new JuegoDAL().CrearPartida(partida);
                FrmJuego frm = new FrmJuego(jugador, partida);

            frm.Show(this);
            Hide();
        }
    }
}
