﻿using BlackJackDAL;
using BlackJackEntities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BlackJack_FabioValerio
{
    public partial class FrmGraficos : Form
    {
        GananciaDAL gDal;
        PerdidaDAL pDal;
        EUsuario usuario;
        public FrmGraficos(EUsuario usu)
        {
            this.usuario = usu;
            gDal = new GananciaDAL();
            pDal = new PerdidaDAL();
            InitializeComponent();
            label1.Text = String.Format("Gráfico Estadístico de {0}", usu.Nombre);
            foreach (EGanancias item in gDal.CargarTodo(usu))
            {
                chart1.Series["Ganancias"].Points.AddXY("", item.ganancia);
            }
            for (int i = 0; i < pDal.CargarTodo(usuario).Count; i++)
            {
                chart1.Series["Perdidas"].Points.AddXY("", pDal.CargarTodo(usuario)[i].perdida);
            }


        }
    }
}
