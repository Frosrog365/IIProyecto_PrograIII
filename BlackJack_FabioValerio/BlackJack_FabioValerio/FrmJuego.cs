﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using Newtonsoft.Json;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BlackJackEntities;
using BlackJackBOL;
using BlackJackDAL;
using System.Speech.Recognition;

namespace BlackJack_FabioValerio
{

    public partial class FrmJuego : Form
    {
        private Mazo mazo;
        private Cartas carta;
        int valor = 0;
        int valorDealer = 0;
        public int apuesta = 0;
        public EUsuario jugador;
        private UsuarioBOL bol;
        private EGanancias ganancia;
        private GananciaDAL ganDAL;
        private EPerdidas perdida;
        private PerdidaDAL perDAL;
        SpeechRecognitionEngine rec = new SpeechRecognitionEngine();
        private bool mic = true;
        public EPartida partida;
        private JuegoBOL log;
        int cont = 0;
        public string urlDealer;
        List<EUsuario> usuarios;

        public FrmJuego()
        {
            InitializeComponent();
        }

        public FrmJuego(EUsuario jugador)
        {
            InitializeComponent();
            this.jugador = jugador;
            partida = new EPartida();
            partida.Password = "";

        }

        public FrmJuego(EUsuario jugador, EPartida partida)
        {
            InitializeComponent();
            this.jugador = jugador;
            this.partida = partida;

        }

        private void FrmJuego_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (Owner != null)
            {
                Owner.Show();
            }

            log.BorrarPartida(jugador);
        }

        private void FrmJuego_Load(object sender, EventArgs e)
        {
            string url = @"https://deckofcardsapi.com/api/deck/new/shuffle/?deck_count=1";
            string json = new WebClient().DownloadString(url);
            mazo = JsonConvert.DeserializeObject<Mazo>(json);
            button1.Enabled = false;
            btnPlantarse.Enabled = false;
            button6.Enabled = true;
            button7.Enabled = false;
            lblFondos.Text = String.Format("Fondos Actuales: ${0}", jugador.Fondos);
            bol = new UsuarioBOL();
            Choices comandos = new Choices();
            comandos.Add(new string[] { "Agarre una carta", "salir del juego", "detenerse" });
            GrammarBuilder gBuilder = new GrammarBuilder();
            gBuilder.Append(comandos);
            Grammar gramtica = new Grammar(gBuilder);
            rec.LoadGrammar(gramtica);
            rec.SetInputToDefaultAudioDevice();
            rec.SpeechRecognized += Reconocimiento;
            log = new JuegoBOL();

            partida = new JuegoBOL().BuscarPublica(jugador, partida);
            if (partida == null)
            {
                MessageBox.Show("Esa partida no existe");
                Close();
                partida = new EPartida();
                partida.Password = "";
                partida = new JuegoBOL().BuscarPublica(jugador, partida);

            }
            else
            {
                button6.Enabled = partida.JugadorActual == jugador.Id;
            }
            timer2.Start();
            usuarios = new List<EUsuario>();
            usuarios.Add(jugador);

        }
        /// <summary>
        /// metodo para refrescar
        /// </summary>
        private void Refrescar()
        {
            partida = log.CargarPartida(partida);
            lblEstado.Text = "Jugando .... " + partida.JugadorActual;//CargarUsuarioID( partida.JugadorActual)
            button6.Enabled = partida.JugadorActual == jugador.Id;
            pictureBox3.ImageLocation = new JuegoDAL().SacarCarta(jugador);
            if (usuarios.Count > 2)
            {
                pictureBox4.ImageLocation = new JuegoDAL().SacarCarta(usuarios[2]);
            }
            if (new JuegoDAL().SacarCartaDealer() != null)
            {
                if (valorDealer <= 16)
                {
                    if (new JuegoDAL().SacarCartaDealer().value.Equals("ACE") && valorDealer <= 10)
                    {
                        valorDealer += 11;
                    }
                    else if (new JuegoDAL().SacarCartaDealer().value.Equals("ACE") && valorDealer > 10)
                    {
                        valorDealer += 1;
                    }
                    else if (new JuegoDAL().SacarCartaDealer().value.Equals("QUEEN"))
                    {
                        valorDealer += 10;
                    }
                    else if (new JuegoDAL().SacarCartaDealer().value.Equals("JACK"))
                    {
                        valorDealer += 10;
                    }
                    else if (new JuegoDAL().SacarCartaDealer().value.Equals("KING"))
                    {
                        valorDealer += 10;
                    }
                    else
                    {
                        valorDealer += int.Parse(new JuegoDAL().SacarCartaDealer().value);
                    }
                    pictureBox2.ImageLocation = new JuegoDAL().SacarCartaDealer().image;
                    label2.Text = "El dealer tiene: " + valorDealer.ToString();

                }
                else
                {
                    if (ganar())
                    {
                        timer1.Stop();
                        MessageBox.Show("HA GANADO!!");
                        jugador.Fondos += apuesta;
                        bol.ModificarFondos(jugador);
                        ganancia = new EGanancias();
                        ganDAL = new GananciaDAL();
                        ganancia.Id_usu = jugador.Id;
                        ganancia.ganancia = apuesta * 2;
                        ganDAL.Insertar(ganancia);
                        reiniciar();
                        log.Plantarse(partida);

                    }
                    else
                    {
                        timer1.Stop();
                        MessageBox.Show("HA PERDIDO!!");
                        jugador.Fondos -= apuesta;
                        bol.ModificarFondos(jugador);
                        perdida = new EPerdidas();
                        perDAL = new PerdidaDAL();
                        perdida.Id_usu = jugador.Id;
                        perdida.perdida = apuesta;
                        perDAL.Insertar(perdida);
                        reiniciar();
                        log.Plantarse(partida);
                    }
                }
            }
        }

        /// <summary>
        /// Metodo para poder utilizar el microfono
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Reconocimiento(object sender, SpeechRecognizedEventArgs e)
        {

            switch (e.Result.Text)
            {

                case "Agarre una carta":
                    PedirCarta();
                    break;
                case "detenerse":
                    timer1.Start();
                    button1.Enabled = false;
                    rec.RecognizeAsyncStop();
                    break;
                case "salir del juego":
                    Close();
                    break;

            }
        }

        private void FrmJuego_FormClosed(object sender, FormClosedEventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {

            PedirCarta();
        }

        /// <summary>
        /// Metodo para pedir carta
        /// </summary>
        private void PedirCarta()
        {
            string urlCarta = String.Format("https://deckofcardsapi.com/api/deck/{0}/draw/?count=1", mazo.deck_id);
            string img = new WebClient().DownloadString(urlCarta);
            carta = JsonConvert.DeserializeObject<Cartas>(img);
            pictureBox1.Load(carta.cards[0].image);
            if (partida.JugadorActual == jugador.Id)
            {
                new JuegoDAL().InsertarCarta(carta.cards[0], jugador);
            }

            if (carta.cards[0].value.Equals("ACE") && valor <= 10)
            {
                valor += 11;
            }
            else if (carta.cards[0].value.Equals("ACE") && valor > 10)
            {
                valor += 1;
            }
            else if (carta.cards[0].value.Equals("QUEEN"))
            {
                valor += 10;
            }
            else if (carta.cards[0].value.Equals("JACK"))
            {
                valor += 10;
            }
            else if (carta.cards[0].value.Equals("KING"))
            {
                valor += 10;
            }
            else
            {
                valor += int.Parse(carta.cards[0].value);
            }
            label1.Text = String.Format("Tiene: {0}", valor);
            if (valor > 21)
            {
                button1.Enabled = false;
                rec.RecognizeAsyncStop();

            }

        }
        /// <summary>
        /// Metodo para pedir carta del dealer
        /// </summary>
        private void PedirCartaDealer()
        {
            string urlCarta = String.Format("https://deckofcardsapi.com/api/deck/{0}/draw/?count=1", mazo.deck_id);
            string img = new WebClient().DownloadString(urlCarta);
            carta = JsonConvert.DeserializeObject<Cartas>(img);
            new JuegoDAL().InsertarCartaDealer(carta.cards[0]);


        }

        /// <summary>
        /// Metodo para reiniciar el tablero
        /// </summary>
        private void reiniciar()
        {
            valor = 0;
            valorDealer = 0;
            string url = @"https://deckofcardsapi.com/api/deck/new/shuffle/?deck_count=1";
            string json = new WebClient().DownloadString(url);
            mazo = JsonConvert.DeserializeObject<Mazo>(json);
            pictureBox1.Image = null;
            label1.Text = String.Format("Tiene: {0}", valor);
            pictureBox2.Image = null;
            label2.Text = String.Format("El Dealer Tiene: {0}", valorDealer);
            button1.Enabled = false;
            btnPlantarse.Enabled = false;
            button6.Enabled = true;
            button7.Enabled = false;
            apuesta = 0;
            label3.Text = String.Format("Apuesta: {0}", apuesta);
            lblFondos.Text = String.Format("Fondos Actuales: ${0}", jugador.Fondos); rec.RecognizeAsyncStop();
            button7.BackgroundImage = global::BlackJack_FabioValerio.Properties.Resources.if_icon_ios7_mic_211772;
            mic = true;
            button3.Enabled = true;
            button4.Enabled = true;
            button5.Enabled = true;
            lblSeleccione.Visible = true;
            new JuegoDAL().ResetearPartida();
            timer1.Stop();
        }

        private void button2_Click(object sender, EventArgs e)
        {

            //timer1.Start();
            btnPlantarse.Enabled = false;
            button1.Enabled = false;
            cont++;
            log.Plantarse(partida);
            List<int> listaJugadores = new JuegoDAL().SacarJugadores();

            if (listaJugadores.Count > 0 && jugador.Id.Equals(listaJugadores.Last()))
            {
                timer1.Start();
            }


        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (valorDealer <= 16)
            {
                PedirCartaDealer();

            }
            else
            {
                if (ganar())
                {
                    timer1.Stop();
                    MessageBox.Show("HA GANADO!!");
                    jugador.Fondos += apuesta;
                    bol.ModificarFondos(jugador);
                    ganancia = new EGanancias();
                    ganDAL = new GananciaDAL();
                    ganancia.Id_usu = jugador.Id;
                    ganancia.ganancia = apuesta * 2;
                    ganDAL.Insertar(ganancia);
                    reiniciar();
                    log.Plantarse(partida);

                }
                else
                {
                    timer1.Stop();
                    MessageBox.Show("HA PERDIDO!!");
                    jugador.Fondos -= apuesta;
                    bol.ModificarFondos(jugador);
                    perdida = new EPerdidas();
                    perDAL = new PerdidaDAL();
                    perdida.Id_usu = jugador.Id;
                    perdida.perdida = apuesta;
                    perDAL.Insertar(perdida);
                    reiniciar();
                    log.Plantarse(partida);
                }

            }

        }

        /// <summary>
        /// Metodo para saber si un usuario ganó o o no
        /// </summary>
        /// <returns></returns>
        private bool ganar()
        {
            if (valorDealer == 21)
            {
                return false;
            }
            else if (valor > 21)
            {
                return false;
            }
            else if ((21 - valorDealer < 21 - valor) && valorDealer < 21)
            {
                return false;


            }
            else if (valorDealer > 21)
            {
                return true;
            }



            else if (valor == 21 && valorDealer != 21)
            {
                return true;
            }
            else if (valor == valorDealer)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void button6_Click(object sender, EventArgs e)
        {
            lblSeleccione.Visible = false;
            if (VerificarApuesta())
            {
                MessageBox.Show("Su apuesta no puede ser mayor a sus fondos");
                apuesta = 0;
                label3.Text = String.Format("Apuesta: {0}", apuesta);
            }
            else
            {
                button1.Enabled = true;
                btnPlantarse.Enabled = true;
                button6.Enabled = false;
                button7.Enabled = true;
                button3.Enabled = false;
                button4.Enabled = false;
                button5.Enabled = false;
            }
        }

        /// <summary>
        /// Metodo para verificar si la apuesta es correcta
        /// </summary>
        /// <returns></returns>
        private bool VerificarApuesta()
        {
            if (apuesta > jugador.Fondos)
            {
                return true;

            }
            else
            {
                return false;
            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            apuesta += 100;
            label3.Text = String.Format("Apuesta: {0}", apuesta);

        }

        private void button3_Click(object sender, EventArgs e)
        {
            apuesta += 500;
            label3.Text = String.Format("Apuesta: {0}", apuesta);
        }

        private void button4_Click(object sender, EventArgs e)
        {
            apuesta += 1000;
            label3.Text = String.Format("Apuesta: {0}", apuesta);
        }

        private void button7_Click(object sender, EventArgs e)
        {
            if (mic)
            {
                rec.RecognizeAsync(RecognizeMode.Multiple);
                button7.BackgroundImage = global::BlackJack_FabioValerio.Properties.Resources.if_icon_ios7_mic_off_211770;
                mic = false;
            }
            else
            {
                rec.RecognizeAsyncStop();
                button7.BackgroundImage = global::BlackJack_FabioValerio.Properties.Resources.if_icon_ios7_mic_211772;
                mic = true;
            }
        }

        private void timer2_Tick(object sender, EventArgs e)
        {



            Refrescar();


        }

        private void pictureBox4_Click(object sender, EventArgs e)
        {

        }
    }
}
