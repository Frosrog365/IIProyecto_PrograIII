﻿using BlackJackBOL;
using BlackJackEntities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace BlackJack_FabioValerio
{
    public partial class BlackJackMenu : Form
    {
        
        UsuarioBOL bol;
        EUsuario jugador;
        string foto;


        public BlackJackMenu()
        {
            InitializeComponent();
        }
        public BlackJackMenu(EUsuario usu, string foto)
        {
            InitializeComponent();
            this.jugador = usu;
            this.foto = foto;
        }

        private void BlackJackMenu_Load(object sender, EventArgs e)
        {
            bol = new UsuarioBOL();
            pbxFoto.ImageLocation = foto;
            lblNombre.Text = String.Format("Bienvenido {0} a BlackJack v.01", jugador.Nombre);
            lblEmail.Text = jugador.Correo;
            jugador.Fondos = bol.cargarFondos(jugador);
            bol.Verificar(jugador);

        }

        private void BlackJackMenu_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (Owner != null)
            {
                Owner.Show();
            }
            jugador.Jugando = false;
            new UsuarioBOL().DejarJugar(jugador);

        }

        private void button1_Click(object sender, EventArgs e)
        {
            FrmJuego frm = new FrmJuego(jugador);
            frm.Show(this);
            Hide();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            FrmMenuFondos frm = new FrmMenuFondos(jugador);
            frm.ShowDialog(this);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            FrmGraficos frm = new FrmGraficos(jugador);
            frm.Show();
        }

        private void button1_MouseEnter(object sender, EventArgs e)
        {
            button1.ForeColor = System.Drawing.Color.Gray;
        }

        private void button1_MouseLeave(object sender, EventArgs e)
        {
            button1.ForeColor = System.Drawing.Color.White;
        }

        private void button2_MouseEnter(object sender, EventArgs e)
        {
            button2.ForeColor = System.Drawing.Color.Gray;
        }

        private void button2_MouseLeave(object sender, EventArgs e)
        {
            button2.ForeColor = System.Drawing.Color.White;
        }

        private void button3_MouseEnter(object sender, EventArgs e)
        {
            button3.ForeColor = System.Drawing.Color.Gray;
        }

        private void button3_MouseLeave(object sender, EventArgs e)
        {
            button3.ForeColor = System.Drawing.Color.White;
        }

        private void button4_MouseEnter(object sender, EventArgs e)
        {
            button4.ForeColor = System.Drawing.Color.Gray;
        }

        private void button4_MouseLeave(object sender, EventArgs e)
        {
            button4.ForeColor = System.Drawing.Color.White;
        }

        private void button5_Click(object sender, EventArgs e)
        {
            CrearPartida frm = new CrearPartida(jugador);
            frm.Show(this);
        }

        private void button6_Click(object sender, EventArgs e)
        {
            UnirseFrm frm = new UnirseFrm(jugador);
            frm.Show(this);
        }
    }
}
