﻿using BlackJackEntities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BlackJack_FabioValerio
{
    public partial class UnirseFrm : Form
    {
        EUsuario jugador;
        

        public UnirseFrm(EUsuario usuario)
        {
            InitializeComponent();
            jugador = usuario;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            EPartida partida = new EPartida
            {
                
                Password = textBox1.Text
            };
            FrmJuego frm = new FrmJuego(jugador, partida);
            frm.Show(this);
            Hide();
        }
    }
}
