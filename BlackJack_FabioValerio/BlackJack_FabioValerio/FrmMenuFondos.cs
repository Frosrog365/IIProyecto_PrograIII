﻿using BlackJackBOL;
using BlackJackEntities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BlackJack_FabioValerio
{
    public partial class FrmMenuFondos : Form
    {
        EUsuario jugador;
        UsuarioBOL bol;
        public FrmMenuFondos()
        {
            InitializeComponent();
        }

        public FrmMenuFondos(EUsuario jugador)
        {
            InitializeComponent();
            this.jugador = jugador;
        }

        private void FrmMenuFondos_Load(object sender, EventArgs e)
        {
            bol = new UsuarioBOL();
            jugador.Fondos = bol.cargarFondos(jugador);
            lblNombre.Text = jugador.Nombre;
            lblFondos.Text += " $"+jugador.Fondos.ToString();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            int num = 0;
            if(int.TryParse(textBox1.Text, out num))
            {
                jugador.Fondos += num;
                lblFondos.Text = "Fondos actuales: $" + jugador.Fondos.ToString();
                textBox1.Text = "";
            }
            else
            {
                MessageBox.Show("Debe escribir solo números");
            }
        }
    }
}
